[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date       | Host                        | Presenter 1     | Presenter 2     | Presenter 3      | 
| ---------- | --------------------------- | --------------- | --------------- | ---------------- |  
| 2023-01-25 | Marcel van Remmerden | Dan Mizzi-Harris   | Pedro Moreira da Silva |                 |
| 2023-02-08 | Justin Mandell       | Mike Nichols       | Gina Doyle             |  Camellia Yang |
| 2023-02-22 | APAC                 | Michael Le         | Katie Macoy            |                 |
| 2023-03-08 | Taurie Davis         | Austin Regnery     | Matt Nearents          | Nick Leonard    |
| 2023-03-22 | Chris Micek          | Sascha Eggenberger | Philip Joyce           | Libor Vanc      |
| 2023-04-05 | Blair Christopher    | Andy Volpe         | Emily Sybrant          | Becka Lippert   |
| 2023-04-19 | Rayana Verissimo     | Ali Ndlovu         | Sunjung Park           |                 |
| 2023-05-03 | Jacki Bauer          | Alexis Ginsberg    | Nick Brandt            | Daniel Mora     |
| 2023-05-17 | Marcel van Remmerden | Matej Latin        | Kevin Comoli           |                 |
| 2023-05-31 | Justin Mandell       | Michael Fangman    | Alexis Ginsberg        | Amelia Bauerly  |
| 2023-06-14 | APAC                 | Alex Fracazo | Katie Macoy            |                 |
| 2023-06-28 | Taurie Davis         | Emily Bauman       | Austin Regnery         |                 |
| 2023-07-12 | Chris Micek          | Dan Mizzi-Harris   | Pedro Moreira da Silva |                 |
| 2023-07-26 | Blair Christopher    | Annabel Gray       | Jeremy Elder           | Mike Nichols    |
| 2023-08-09 | Rayana Verissimo     | Sascha Eggenberger | Philip Joyce           |  Camellia Yang |
| 2023-08-23 | Jacki Bauer          | Gina Doyle         | Veethika Mishra        | Matt Nearents   |
| 2023-09-06 | Marcel van Remmerden | Ali Ndlovu         | Sunjung Park           | Libor Vanc      |
| 2023-09-20 | Justin Mandell       | Nick Leonard       | Andy Volpe             | Emily Sybrant   |
| 2023-10-04 | APAC                 | Michael Le         | Alex Fracazo       |                 |
| 2023-10-18 | Taurie Davis         | Becka Lippert      | Alexis Ginsberg        | Nick Brandt     |
| 2023-11-01 | Chris Micek          | Matej Latin        | Kevin Comoli           |                 |
| 2023-11-15 | Blair Christopher    | Daniel Mora        | Michael Fangman        | Alexis Ginsberg |
| 2023-11-29 | Rayana Verissimo     |                    |                        |                 |
| 2023-12-13 | Jacki Bauer          | Amelia Bauerly     | Emily Bauman           | Jeremy Elder           |
